import express from 'express'
import { v4 as uuidv4 } from 'uuid'
import responseTime from 'response-time'
import { createClient } from 'redis'
const app = express()

const client = createClient({
  host: '127.0.0.1',
  port: 6379
})

app.use(express.json())
app.use(responseTime())

app.get('/', (req, res) => {
  res.status(200).json({
    response: 'api-express-redis'
  })
})

app.get('/store/:id', async (req, res) => {
  const { id } = req.params

  const getKey = await client.hGetAll(id) // handleError

  res.status(200).json({
    id,
    getKey
  })
})

app.put('/store/:id', async (req, res) => {
  const { id } = req.params
  const dataToInser = req.body // handle object type format

  // Refactor logic to handleError (prevent crash-app)
  try {
    await client.hSet(id, dataToInser)

    res.status(200).json({
      status: '200',
      id
    })
  } catch (err) {
    res.status(500).json(err)
  }
})

app.post('/store/create', async (req, res) => {
  const id = uuidv4()

  const dataToInser = req.body // handle object type format

  // Refactor logic to handleError (prevent crash-app)
  try {
    await client.hSet(id, dataToInser)

    res.status(200).json({
      status: '200',
      id
    })
  } catch (err) {
    res.status(500).json(err)
  }
})

client.on('connect', function () {
  console.log('connected')
})

client.on('error', (err) => console.log('Redis Client Error', err))

app.listen(4000, () => {
  console.log('[server]: port 4000')
  client.connect()
})
